#ifndef MONITOR_H
#define MONITOR_H

//#include "EngineHowTo.h"
#include "CommonDataModel.h"
#include "PulsePhysiologyEngine.h"

#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <cmath>
#include <libconfig.h++>

#include "MedicalDevice.h"
#include "Controller.h"
#include "Environment.h"
#include "PhysiologyData.h"

#include "system/physiology/SECardiovascularSystem.h"
#include "properties/SEScalarTime.h"
#include "properties/SEScalarPressure.h"
#include "properties/SEScalarVolumePerTime.h"
#include "properties/SEScalarVolume.h"

using namespace std;
using namespace libconfig;

class Monitor : public MedicalDevice {

	private:
		bool is_input_multiple;
		bool is_output_multiple;
		bool HeartRate;
	  bool BloodPressure;

		double m_output_period_secs;
		double m_input_period_secs;
		double time_step;
		double heart_rate;

		unsigned int input_period;
		unsigned int output_period;

		CMD::Environment* m_env;
		PhysiologyData* m_data;

	public:

	    // Constructor
	    Monitor();
	    Monitor(CMD::Environment*, PhysiologyData*);

	    // Device logic will go in here
	    // This function uses the engine object to query required info
	    virtual void update(std::unique_ptr<PhysiologyEngine>&);

	    // Load the configuration file to capture device rate
	    virtual void LoadConfig(std::unique_ptr<PhysiologyEngine>&,const std::shared_ptr<Config>&);

	    // Method to transfer physiological data to Controller
	    void SendData(PhysiologyData&);

	    void Stop();



	    // Destructor
	    ~Monitor();
};

#endif
