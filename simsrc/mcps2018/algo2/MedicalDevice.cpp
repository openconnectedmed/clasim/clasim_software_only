#include "MedicalDevice.h"


// Constructor
MedicalDevice::MedicalDevice() :
	m_input_period_secs(0.02), MIN_PERIOD_SECS(0.02) // Initialize member variables
{

};


// Use rate provided in config file; Default is 1 from Constructor initialization
// Engine object will be utilized here to query data
void MedicalDevice::update(std::unique_ptr<PhysiologyEngine>& engine) {

};

// Extract the rate from the configuration file
// str: path to configuration file
// Pure virtual function to be implemented in derived classes
void
MedicalDevice::LoadConfig(std::unique_ptr<PhysiologyEngine>&, const std::shared_ptr<Config>& cf){

};


// Destructor
MedicalDevice::~MedicalDevice()
{

};
